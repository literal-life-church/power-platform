# Power Platform

- **Media:** Manages when our videos become publicly available on OneDrive for user consumption and how they are structured.
    - **Send Video and Audio Files for Approval:** Invokes a Power Automate approval flow to determine whether a particular recording should be publicly published, or kept unpublished in the archives, and then places it in the corresponding folder based on the publication request and relevant year. Also allows the approver to use a comment to update the file name prior to moving it.
